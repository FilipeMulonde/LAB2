#define BOOST_TEST_MODULE LAB2_TESTS
#include <boost/test/included/unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"

BOOST_AUTO_TEST_SUITE(rectangle_tests)
BOOST_AUTO_TEST_CASE(rectangle_move_Method_Whit_dx_dy)
{
  Rectangle rectangle(11, -13 , 5, 5);
  const double width  = rectangle.getFrame().width;
  const double height = rectangle.getFrame().height;
  const double area   = rectangle.getArea();
  rectangle.move(5.33, 42.55);
  BOOST_CHECK_CLOSE(width,  rectangle.getFrame().width, 0.002);
  BOOST_CHECK_CLOSE(height, rectangle.getFrame().height, 0.002);
  BOOST_CHECK_CLOSE(area,   rectangle.getArea(), 0.002);
}
BOOST_AUTO_TEST_CASE(rectangle_move_Method_Whit_point)
{
  Rectangle rectangle(-11, 157.7 , 16.9, 4.59);
  const double width = rectangle.getFrame().width;
  const double height= rectangle.getFrame().height;
  const double area  = rectangle.getArea();
  rectangle.move(-1.82, 87.14);
  BOOST_CHECK_CLOSE(width, rectangle.getFrame().width, 1.002);
  BOOST_CHECK_CLOSE(height,rectangle.getFrame().height, 1.002);
  BOOST_CHECK_CLOSE(area,  rectangle.getArea(), 2.001);
}
BOOST_AUTO_TEST_CASE(rectangle_scale)
{
  Rectangle rectangle(14,16, 10, 7);
  const double area = rectangle.getArea();
  const double kf = 2.2;
  rectangle.scale(kf);
  BOOST_CHECK_CLOSE(area * kf * kf, rectangle.getArea(), 3.001);
}
BOOST_AUTO_TEST_CASE(rectangle_invalid_parameters)
{
  BOOST_CHECK_THROW(Rectangle( 2, 2 , -3, -2), std::invalid_argument);
  Rectangle rectangle(1, 5 , 10, 12);
  BOOST_CHECK_THROW(rectangle.scale(-3), std::invalid_argument);
}
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(circle_tests)
BOOST_AUTO_TEST_CASE(circle_move_method_whit_dx_dy)
{
  Circle circle(-4, 47, 50);
  const double width   = circle.getFrame().width;
  const double height  = circle.getFrame().height;
  const double area    = circle.getArea();
  circle.move(5.51, 0);
  BOOST_CHECK_CLOSE(width,  circle.getFrame().width, 0.003);
  BOOST_CHECK_CLOSE(height, circle.getFrame().height, 0.003);
  BOOST_CHECK_CLOSE(area,   circle.getArea(), 0.002);
}
BOOST_AUTO_TEST_CASE(circle_move_method_whit_point)
{
  Circle circle(-64, 347.5 , 5);
  const double width  = circle.getFrame().width;
  const double height = circle.getFrame().height;
  const double area = circle.getArea();
  circle.move({ 2.41, -57.51 });
  BOOST_CHECK_CLOSE(width, circle.getFrame().width, 0.001);
  BOOST_CHECK_CLOSE(height, circle.getFrame().height, 0.001);
  BOOST_CHECK_CLOSE(area,  circle.getArea(), 0.001);
}
BOOST_AUTO_TEST_CASE(circle_scale)
{
  Circle circle(23,43 , 9);
  const double area = circle.getArea();
  const double kf = 0.3;
  circle.scale(kf);
  BOOST_CHECK_CLOSE(area * kf * kf, circle.getArea(), 0.004);
}
BOOST_AUTO_TEST_CASE(circle_invalid_parameters)
{
  BOOST_CHECK_THROW(Circle(2, 2, -1), std::invalid_argument);
  Circle circle(1, 5, 3);
  BOOST_CHECK_THROW(circle.scale(-3), std::invalid_argument);
}
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(triangle_tests)
BOOST_AUTO_TEST_CASE(triangle_move_method_whit_dx_dy)
{
  Triangle triangle({ -5, 24 }, { 2,-3 }, { 15,41 });
  const double width = triangle.getFrame().width;
  const double height = triangle.getFrame().height;
  const double area = triangle.getArea();
  triangle.move(-4.16, 5.27);
  BOOST_CHECK_CLOSE(width, triangle.getFrame().width, 0.003);
  BOOST_CHECK_CLOSE(height,triangle.getFrame().height, 0.401);
  BOOST_CHECK_CLOSE(area,  triangle.getArea(), 0.005);
}
BOOST_AUTO_TEST_CASE(triangle_move_method_whit_point)
{
  Triangle triangle({ -14, 4.4 }, { 5, -24 }, { 4.55, 9.6 });
  const double width = triangle.getFrame().width;
  const double height = triangle.getFrame().height;
  const double area = triangle.getArea();
  triangle.move({ -35.5, 5.58 });
  BOOST_CHECK_CLOSE(width, triangle.getFrame().width, 3.001);
  BOOST_CHECK_CLOSE(height,triangle.getFrame().height, 0.001);
  BOOST_CHECK_CLOSE(area, triangle.getArea(), 0.004);
}
BOOST_AUTO_TEST_CASE(triangle_scale)
{
  Triangle triangle({ 12,14 }, { 3, 52 }, { 32, 1 });
  const double area = triangle.getArea();
  const double kf = 2.8;
  triangle.scale(kf);
  BOOST_CHECK_CLOSE(area * kf * kf, triangle.getArea(), 0.009);
}
BOOST_AUTO_TEST_CASE(triangle_invalid_parameters)
{
  BOOST_CHECK_THROW(Triangle({ 1, 0 }, { 1, 0 }, { 5, 0 }), std::invalid_argument);
  Triangle triangle({ 1, 5 }, { 5, 1 }, { 0, 0 });
  BOOST_CHECK_THROW(triangle.scale(-15), std::invalid_argument);
}
BOOST_AUTO_TEST_SUITE_END()
