#include "rectangle.hpp"
#include <iostream>
#include <stdexcept>

Rectangle::Rectangle(double centerX, double centerY, double width, double height) :
  rectangle_({centerX, centerY, width, height })
{
  if(width <= 0 || height <= 0)
  {
    throw std::invalid_argument("Invalid Rectangle parameters!,the 2 parameters should be Greater then 0");
  }
}
double Rectangle::getArea() const
{
	return rectangle_.width * rectangle_.height;
}
rectangle_t Rectangle::getFrame() const
{
	return rectangle_;
}
void Rectangle::move(double dx, double dy)
{
	rectangle_.pos.x += dx;
	rectangle_.pos.y += dy;
}
void Rectangle::move(point_t p)
{
  rectangle_.pos = p;
}

void Rectangle::scale(const double kf)
{
  if(kf <= 0)
  {
    throw std::invalid_argument("Invalid Rectangle scale coefficient, should be Greater then 0");
  }

    rectangle_.height *= kf;
    rectangle_.width *= kf;

}
void Rectangle::show() const
{
	std::cout << "Area of a Rectangle: " << getArea() << std::endl;
	std::cout << "Frame X : " << rectangle_.pos.x << " Frame Y: " << rectangle_.pos.y << std::endl;
	std::cout << "Frame -Width: " << rectangle_.width << ";Frame Height: " << rectangle_.height << "\n\n";
}
