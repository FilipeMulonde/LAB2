#include "triangle.hpp"
#include "base-types.hpp"
#include <algorithm>
#include <iostream>
#include <stdexcept>


Triangle::Triangle(point_t p1,point_t p2,point_t p3) :
  a_(p1),
  b_(p2),
  c_(p3)
{
  if(getArea() <= 0)
  {
    throw std::invalid_argument("Invalid triangle parameters!the 3 parameters should be Greater then 0");
  }
}
double Triangle::getArea() const
{
	double ab = findDistance(a_, b_);
	double ac = findDistance(a_, c_);
	double bc = findDistance(b_, c_);
	double s = (ab + ac + bc) / 2;
	return  sqrt(s*(s - ab)*(s - ac)*(s - bc));
}
rectangle_t Triangle::getFrame() const
{
	 using namespace std;
		double left   =  min(min(a_.x, b_.x),  c_.x);
		double bottom =  min(min(a_.y, b_.y),   c_.y);
		double height =  abs(max(max(a_.y, b_.y), c_.y) - bottom);
		double width  =  abs(max(max(a_.x, b_.x), c_.x) - left);
		return rectangle_t{ point_t{left + width / 2, bottom + height / 2}, width, height};
}
void Triangle::move(double dx, double dy)
{
	a_.x += dx;
	a_.y += dy;
	b_.x += dx;
	b_.y += dy;
	c_.x += dx;
	c_.y += dy;
}
void Triangle::move(point_t p)
{
	move(p.x - findCenter().x, p.y - findCenter().y);
}
point_t Triangle::findCenter() const
{
	return point_t{ (a_.x + b_.x + c_.x) / 3, (a_.y + b_.y + c_.y) / 3 };
}
double Triangle::findDistance(point_t p1, point_t p2) const
{
	return sqrt((p2.x - p1.x)*(p2.x - p1.x) + (p2.y - p1.y)*(p2.y - p1.y));
}
void Triangle::scale(const double kf)
{
  if(kf <= 0)
  {
    throw std::invalid_argument("Invalid Triangle scale coefficient!");
  }
  else
  {
    point_t center = findCenter();
    a_.x = center.x + (a_.x - center.x) * kf;
    a_.y = center.y + (a_.y - center.y) * kf;
    b_.x = center.x + (b_.x - center.x) * kf;
    b_.y = center.y + (b_.y - center.y) * kf;
    c_.x = center.x + (c_.x - center.x) * kf;
    c_.y = center.y + (c_.y - center.y) * kf;
  }
}

void Triangle::show() const
{
	std::cout << "Area of a Triangle: " << Triangle::getArea() << std::endl;
	point_t center_ = findCenter();
	std::cout << "Center of a Triangle: " << " X: " << center_.x << " Y: " << center_.y << std::endl;
	rectangle_t frame = getFrame();
	std::cout << "Frame X : " << frame.pos.x << ";Frame Y: " << frame.pos.y << std::endl;
	std::cout << "Frame -Width: " << frame.width << ";Frame Height: " << frame.height << "\n\n";
}
